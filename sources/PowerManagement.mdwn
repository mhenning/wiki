The following page contains information about the current state of PM, examples of how to use it and possible ways to determine if it may be used on your system. 

It is currently only possible to read temperature and performance levels on several cards, as long as you have a recent enough kernel (nouveau git from October 2010 or 2.6.37), and a supported card. 

Please note that this page is only a rough approximation of the current state of PM. 


## Key

* "**DONE**" means... that it is thought to be fully implemented. May contain bugs. 
* "**MOSTLY**" means that it is mostly implemented and may have known bugs. 
* "**WIP**" means that someone has started on the initial implementation. 
* "**TODO**" means that someone needs to write the code. The required knowledge to write the code may or may not be known. Please ask on #nouveau if you want to get your feet wet on this. 
* "**STALLED**" means... that whatever code has been written is accumulating color and texture similar to that 3 week old slice of pizza in your fridge. 

## Status Matrix
[[!table header="no" class="mointable" data="""
 **Chipset** || | **[[NV04/05|CodeNames]]** | **[[NV10|CodeNames]]** | **[[NV20|CodeNames]]** | **[[NV30|CodeNames]]** | **[[NV40|CodeNames]]** | **[[NV50|CodeNames]]** | **[[NVC0|CodeNames]]** | **[[NVE0|CodeNames]]** | **[[NV110|CodeNames]]** | **[[NV120|CodeNames]]** | **[[NV130|CodeNames]]** | **[[NV140|CodeNames]]** | **[[NV160|CodeNames]]** | **[[NV170|CodeNames]]**
**VBIOS tables parsing** || |   |   |   |   |   |   |   |   |   |   |   |   |   |
Performance levels|| | N/A | MOSTLY | MOSTLY | MOSTLY | MOSTLY | MOSTLY | MOSTLY | MOSTLY | MOSTLY | MOSTLY | TODO | TODO | TODO | TODO
Voltage|| | N/A | N/A | N/A | MOSTLY | MOSTLY | MOSTLY | MOSTLY | MOSTLY | MOSTLY | MOSTLY | TODO | TODO | TODO | TODO
Thermal settings|| | N/A | N/A | N/A | MOSTLY | MOSTLY | MOSTLY | MOSTLY | MOSTLY | MOSTLY | MOSTLY | TODO | TODO | TODO | TODO
Memory timings|| | N/A | ? | ? | STALLED | STALLED | MOSTLY | DONE | DONE | MOSTLY | MOSTLY | TODO | TODO | TODO | TODO
Mem clock/timing mapping|| | N/A | N/A | N/A | N/A | DONE | DONE | DONE | DONE | MOSTLY | MOSTLY | TODO | TODO | TODO | TODO
**Power Management** || |   |   |   |   |   |   |   |   |   |   |   |   |   |
Engine reclocking || | N/A | N/A | N/A | MOSTLY | MOSTLY | MOSTLY | MOSTLY | WIP | TODO | TODO | TODO | TODO | TODO | TODO
Memory reclocking || | N/A | MOSTLY | MOSTLY | MOSTLY | MOSTLY | MOSTLY | WIP | WIP | TODO | TODO | TODO | TODO | TODO | TODO
Voltage adjusting|| | N/A | N/A | N/A | internal only | internal only | internal only | internal only | internal only | internal only | TODO | TODO | TODO | TODO | TODO
Thermal sensors|| | N/A | N/A | DONE<sup>1</sup> | DONE<sup>1</sup> | DONE<sup>1</sup> | DONE<sup>1</sup> | DONE<sup>1</sup> | DONE | DONE | DONE | TODO | TODO | TODO | TODO
Fanspeed control|| | N/A | N/A | N/A | DONE | DONE<sup>2</sup> | DONE<sup>2</sup> | DONE<sup>2</sup> | DONE | DONE | TODO<sup>3</sup> | TODO | TODO | TODO | TODO
"""]]

### Notes

 - <sup>1</sup>: I²C temperature probes cannot always be read from Nouveau's hwmon interface.
 - <sup>2</sup>: I²C fans cannot be managed with Nouveau's hwmon interface.
 - <sup>3</sup>: For GM20x reclocking we can't control the fan without high-secure firmware, so you have to have some fan connected that's not controlled internally by the GPU.

## Docs

Some documentation related to power management on NVIDIA GPUs have been published as part of a research paper named [[Reverse Engineering Power Management on NVIDIA GPUs - a Detailed Overview|http://phd.mupuf.org/publication/2013/09/25/reverse-engineering-power-management-on-nvidia-gpus/]].
