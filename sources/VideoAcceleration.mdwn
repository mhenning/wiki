Most NVIDIA cards have built-in video codec decoding of some sort, and it can in many cases be used by linux via XvMC or VDPAU. This is usually marketed as PureVideo or PureVideo HD. Note that this page is not about Xv, which is essentially an overlay mechanism (and whose status you can see on the [[FeatureMatrix]]).

Unfortunately the different video decoding acceleration engines don't map very nicely to card generations, so we split this page into 2 parts: which cards have what video engine, and then which video engines support what. A more detailed analysis of these may be found at [[VideoDecoding]].

## Which card has what engine

All ranges are inclusive. Some cards contain multiple engines (esp pre-NVA0 chips). See [[CodeNames]] for the full marketing name list. Note that NV50 below doesn't refer to the family but rather the exact chip. One way to find out which chip you have is by running `dmesg | grep -i chipset`.

* VPE1: NV17, NV18, NV1F, NV30-NV96, NVA0 (GeForce4 MX, 5, 6, 7, 8, 9 series)
* VPE2: NV31, NV34, NV36, NV40-NV96, NVA0 (GeForce 5 (some), 6, 7, 8, 9 series)
* VP1: NV41-NV50 (GeForce 6, 7 series, GeForce 8800 GTS)
* VP2: NV84-NV96, NVA0 (GeForce 8, 9 series; corresponds to VDPAU feature set A)
* VP3: NV98, NVAA, NVAC (GeForce 9300, GeForce IGPs; corresponds to VDPAU feature set B)
* VP4.0: NVA3-NVA8, NVAF (GeForce 200, 300 series; corresponds to VDPAU feature set C)
* VP4.2: NVC0-NVCF (GeForce 400, 500 series; corresponds to VDPAU feature set C)
* VP5: NVD0-NV108 (GeForce 600, 700 series, a few low 500s; corresponds to VDPAU feature set D)
* VP6: NV117-NV124 (GeForce 750; GeForce 800, 900 series; corresponds to VDPAU feature set E)
* VP?: NV126 (GeForce 900 series; corresponds to VDPAU feature set F)

## Video engine support status

<style>table.ColorTable td { width: 80px }</style>

[[!table header="no" class="ColorTable" data="""
 | **XvMC** | **VDPAU / VA-API<sup>6</sup>** |||| **Firmware**
 | **MPEG1/2** | **MPEG1/2** | **H.264** | **VC-1** | **MPEG-4** | 
VPE1 | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | N/A | N/A | N/A | N/A
VPE2 | [[!color background=yellow text=MOSTLY<sup>3</sup> ]] | [[!color background=red text=TODO ]] | N/A | N/A | N/A | N/A
VP1 | N/A | N/A | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | N/A | [[!color background=red text=TODO ]]
[[VP2]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE<sup>2&nbsp;4&nbsp;5</sup> ]] | [[!color background=red text=TODO ]] | N/A | [[!color background=slateblue text=EXTFW ]]
[[VP3|VP4]] | N/A<sup>1</sup> | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE<sup>4</sup> ]] | [[!color background=palegreen text=DONE ]] | N/A | [[!color background=slateblue text=EXTFW ]]
[[VP4.0|VP4]] | N/A<sup>1</sup> | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE<sup>4</sup> ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=slateblue text=EXTFW ]]
VP4.2 | N/A<sup>1</sup> | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE<sup>4</sup> ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=slateblue text=EXTFW ]]
VP5 | N/A<sup>1</sup> | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE<sup>4</sup> ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=slateblue text=EXTFW ]]
VP6+ | N/A | [[!color background=red text=TODO]] | [[!color background=red text=TODO]] | [[!color background=red text=TODO]] | [[!color background=red text=TODO]] | [[!color background=red text=TODO]]
"""]]

#### Notes

<sup>1</sup> While XvMC support may be technically possible, these engines are capable of full-stream acceleration (while XvMC only supports IDCT-level acceleration), and using it would be counter-productive.

<sup>2</sup> H.264 videos with field_pic_order=1 set are not supported (should be quite rare, let #nouveau know if you have real-life examples).

<sup>3</sup> Works on NV40+, fails on NV31/34/36.

<sup>4</sup> H.264 videos of movie trailers often end up rendering with some artifacts. Never seen in an actual movie though.

<sup>5</sup> G92 chips appear to hang when decoding H.264 videos. See [[this bug|https://bugs.freedesktop.org/show_bug.cgi?id=82835]] for more info.

<sup>6</sup> VA-API support is introduced in Mesa 11.2. Note that due to a shortcoming in VA-API itself, MPEG-4 can't be properly supported. To enable a hacky workaround, set `VAAPI_MPEG4_ENABLED=true` in the environment.

## Firmware

VPE1/2 do not require any firmware to run. However, VP1/2/3/4/5 all require firmware to operate that is presently extracted from the NVIDIA binary driver. We cannot redistribute the firmware directly in linux-firmware because NVIDIA's license forbids redistribution of parts of their driver.

An effort is underway to fully reverse the underlying engines and create open-source firmware to provide out-of-the-box video decoding, please join us in #nouveau on www.oftc.net if you're interested in helping.

If you are lucky, your distribution may already have a package for installing the firmwares. Here is a list of the supported distribution:

* [Archlinux](https://aur.archlinux.org/packages/nouveau-fw)
* Gentoo: [sys-firmware/nvidia-firmware](http://packages.gentoo.org/package/sys-firmware/nvidia-firmware)
* More to come

If your distribution isn't in the list, you can run the following commands to install the firmware:

    $ mkdir /tmp/nouveau
    $ cd /tmp/nouveau
    $ wget https://raw.github.com/envytools/firmware/master/extract_firmware.py
    $ wget http://us.download.nvidia.com/XFree86/Linux-x86/325.15/NVIDIA-Linux-x86-325.15.run
    $ sh NVIDIA-Linux-x86-325.15.run --extract-only
    $ python2 extract_firmware.py  # this script is for python 2 only
    # mkdir /lib/firmware/nouveau
    # cp -d nv* vuc-* /lib/firmware/nouveau/

You should be ready to use video decoding.

You may alternatively generate the firmware yourself by tracing the blob, you can find instructions at [[NVC0_Firmware]]. Note that you may find some small differences between what the script extracts and what is produced in the mmiotrace for the VP3+ BSP firmware; this is expected.

## Software versions

All acceleration is supported by the kernel and mesa's Gallium3D drivers (driven by its XvMC and VDPAU state trackers). Here are the version requirements for the various engines:

* VPE2:
  * NV50+: Kernel 3.0 (approx), Mesa 8.0. If you want to force use of this engine for NV84+ even if though have VP2, you can set `NOUVEAU_PMPEG=1` in your environment (with Mesa 10.0).
  * NV44+: Kernel 3.11, Mesa 10.0.
  * NV40+: Kernel 3.13, Mesa 10.0.
* VP2: Kernel 3.11, Mesa 9.2. See [[VP2]].
* VP3/VP4.0: Kernel 3.12 and Mesa 10.0.1. See [[VP4]].
* VP4.2: Kernel 3.8, Mesa 9.1
* VP5: Kernel 3.8, Mesa 9.1

## Using VDPAU

Note: If you've compiled a Mesa library and installed it somewhere other than /usr hoping to use LD_LIBRARY_PATH, make sure to install libvdpau into the same prefix. Otherwise your vdpau libs won't get loaded.

To just test things out once, you can do:

    mplayer -vo vdpau -vc ffmpeg12vdpau,ffwmv3vdpau,ffvc1vdpau,ffh264vdpau,ffodivxvdpau, <file>

In order to make these settings more permanent, you can add

    [vo.vdpau]
    vc=ffmpeg12vdpau,ffwmv3vdpau,ffvc1vdpau,ffh264vdpau,ffodivxvdpau,

to your `/etc/mplayer/mplayer.conf`. Then when you use `mplayer -vo vdpau`, it will always use those codecs.

## Using XvMC

Note: There was a problem with Mesa 9.1 (and 9.0) `libXvMCnouveau.so` in that it couldn't load. You can use [[this patch|http://cgit.freedesktop.org/mesa/mesa/commit/?id=4bc8e3c3e45a0086d7a8077bebf2fc50819ff54d]] to fix it up. Mesa 9.2 should be fine again.

You need to point your `/etc/X11/XvMCConfig` at the `libXvMCnouveau.so` library (single line with the full path). Then you should be able to just do

    mplayer -vo xvmc <file>

## Troubleshooting

### Missing libvdpau_nouveau.so

You need to make sure that mesa is compiled with the vdpau state tracker enabled.

### Failed creating VDPAU decoder: A catch-all error

Do you have the firmware installed? Look in `/lib/firmware/nouveau`. You should see a lot of files that look like `nvXX_fucYYY`, and some that start with `vuc-`. If you don't, you forgot to install the firmware, see above. Note that if you find a package called `nouveau-firmware`, it is most likely not the right firmware (it contains ctxprogs for very old versions of nouveau).

### Decodes fine, but video plays back too slowly

The boot clocks on your card may not be fast enough to decode a video of this bitrate/resolution. You can play around with the experimental reclocking support, or wait until it is more available. Sorry!
