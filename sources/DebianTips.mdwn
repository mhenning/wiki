In order to build (Re)nouveau, you must have libSDL 1.2 installed.  The easiest way to do this is using apt-get: 
[[!format txt """
apt-get install libsdl1.2-dev
"""]]
Installing the following will help you build nouveau. 
[[!format txt """
apt-get install xorg-dev libdrm-dev git git-core libtool mesa-common-dev automake autoconf 
"""]]
The Debian nvidia-glx package (at least the one in Etch) stores the original Mesa libglx as `/usr/lib/nvidia/libglx.so.xlibmesa`, which you may want to symlink as `/usr/lib/xorg/modules/extensions/libglx.so` to avoid the undefined symbol error. 

Running git commands (such as `git clone` and `git checkout`) may yield the following message: 


[[!format txt """
If you are looking for git, Linus Torvald's content tracker, install
the cogito and git-core packages and see README.Debian and git(7).

This transition script will be removed in the debian stable
release after etch.

If you wish to complete the transition early, install git-core
and use (as root):
 update-alternatives --config git

Press RETURN to run gitfm
"""]]
If this is the case, run `update-alternatives --config git` as root, and select the `/usr/bin/git-scm` alternative (option 1 in the following example): 


[[!format txt """
%) update-alternatives --config git

There are 2 alternatives which provide `git'.

  Selection    Alternative
-----------------------------------------------
      1        /usr/bin/git-scm
*+    2        /usr/bin/git.transition

Press enter to keep the default[*], or type selection number:
"""]]