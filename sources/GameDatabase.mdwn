[[!table header="no" class="ColorTable" data="""
 **Game**                      | **Chipset** | **Mesa**    | **Kernel(-module)**               | **Performance** | **Notes**
Bioschock Infinite             | GK106       | git-2d140ae | 4.6.3+nouveau-e6224b3+reclocking  | medicore        | stutters, no known graphical issues
Counter Strike                 | GF119       | 11.2.2      | 4.4.0-rc6-NOUVEAU-163068-gc11b898 | very good       | 
Counter Strike: Condition Zero | GF119       | 11.2.2      | 4.4.0-rc6-NOUVEAU-163068-gc11b898 | very good       | 
Civilisation V                 | GK106       | git-cb7c2c9 | 4.7.2+nouveau-d837e75+reclocking  | very good       | CPU bottlenecked
Crusader Kings II              | GK107       | 11.2        | UNK                               | runs fine       | 
F1 2015                        | GF119       | 11.2.2      | 4.4.0-rc6-NOUVEAU-163068-gc11b898 |                 | totally broken (hangs the gpu)
Invisible, Inc                 | GK106       | git-2d140ae | 4.6.3+nouveau-e6224b3+reclocking  | good            | 
Life is Strange                | GK106       | git-2d140ae | 4.6.3+nouveau-e6224b3+reclocking  | good            | 
Payday 2                       | GK106       | git-2d140ae | 4.6.3+nouveau-e6224b3+reclocking  | good            | 
Rogue Legacy                   | GK106       | git-09dafb9 | 4.7.1+nouveau-1d13476+reclocking  | good            | needs BGRA4 disabled
Satellite Reign                | GK106       | git-2d140ae | 4.6.3+nouveau-e6224b3+reclocking  | medicore        | 
Serious Sam 3: BFE             | GF119       | 11.2.2      | 4.4.0-rc6-NOUVEAU-163068-gc11b898 | good            | 
Stellaris                      | GK107       | 11.2        | UNK                               | medicore        | playable as long as you do not look at the galaxy map which makes framerate drop
Tomb Raider 2013               | GK106       | git-2d140ae | 4.6.3+nouveau-e6224b3+reclocking  | good            | 
Team Fortress 2                | GF119       | 11.2.2      | 4.4.0-rc6-NOUVEAU-163068-gc11b898 | good            | 
The Talos Principle            | GF119       | 11.2.2      | 4.4.0-rc6-NOUVEAU-163068-gc11b898 | good            | lot of flickering
Trine                          | GF119       | 11.2.2      | 4.4.0-rc6-NOUVEAU-163068-gc11b898 | good            | 
World of Goo                   | GF119       | 11.2.2      | 4.4.0-rc6-NOUVEAU-163068-gc11b898 | good            | 
"""]]


<style type="text/css">
.na { background-color: #eee; text-align:center; }
.good { background-color: #2e2; text-align:center; }
.ok { background-color: #ee2; text-align:center; }
.bad { background-color: #e22; text-align:center; }
</style>
<table id="reports" cellpadding="0" cellspacing="0">
<thead>
<tr><th>Game</th><th>Tesla</th><th>Fermi</th><th>Kepler</th><th>Maxwell</th><th>Pascal</th></tr>
</thead>
<tbody>
</tbody>
</table>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
var FAMILIES = ["Tesla", "Fermi", "Kepler", "Maxwell", "Pascal"];
var FAMILY_MAP = {
  "GF119": "Fermi"
};

var GAMES = {

"Counter Strike": [
  {
    "reporter": "hakzsam",
    "chipset": "GF119",
    "score": 10,
    "mesa": "11.2.2",
    "kernel": "4.4.0-rc6-NOUVEAU-163068-gc11b898",
    "notes": "very good perf, no know issues"
  }
],



};

$(function() {

var $table = $('#reports tbody');
var games = Object.keys(GAMES);
games.sort();
for (var i = 0; i < games.length; i++) {
  var $row = $('<tr>');
  var game = GAMES[games[i]];
  $row.append($('<td>').text(games[i]));
  var totals = {};
  var counts = {};
  var notes = {};
  for (var r = 0; r < game.length; r++) {
    var report = game[r];
    var f = FAMILY_MAP[report.chipset];
    if (!(f in totals)) { totals[f] = 0; counts[f] = 0; notes[f] = [];}
    totals[f] += report.score;
    counts[f] += 1;
    notes[f].push(report);
  }
  for (var f = 0; f < FAMILIES.length; f++) {
    var fam = FAMILIES[f];
    var $val = $('<td>');
    $row.append($val);
    if (!(fam in totals)) {
      $val.text('?');
      $val.addClass('na');
      continue;
    }
    var score = totals[fam] / counts[fam];
    if (score > 7)
      $val.addClass('good');
    else if (score > 3)
      $val.addClass('ok');
    else
      $val.addClass('bad');
    score = Math.round(score * 10) / 10;
    $val.text("" + score);
    $val.attr('title', 'Reports: ' + notes[fam].length);
  }
  $table.append($row);
}

});
</script>
