**Hardware donations are a great way to help Nouveau developers.** 

However, note that most of the developers have a lot of cards and rarely require new hardware. Very common graphics cards may therefore not be useful. 

If you have hardware you are ready to donate:  

1. Check if it is [[needed by a developer|HardwareDonations]]. 

2. Check if, despite no one asking for it, it [[might be of interest|HardwareDonations]] to someone. 

<a name="explicitelyrequested"></a> 
# Hardware requested by developers

Please look in the table below if someone needs it and is sufficiently close to your location, and contact him (privately or not) to propose the card<sup>1</sup>. 

[[CodeNames|CodeNames]] can help you find out what your card is. If unsure, ask on IRC. 
[[!table header="no" class="mointable" data="""
**Name** | **Location** | **Hardware Wanted**
M. Example |  Grenoble, France |  NV73, NV02
[[Lucas Stach|LucasStach]] |  Germany  |  NV30/NV35, NV44  - contact details can be found on my wiki page , I'm willing to pay for the shipment
Martin Peres | Helsinki, Finland | Any NV3X/4X with a PCI/PCIE port (save NV4B and NV43), NVA[358] with GDDR3 or 5 memory, NVAA/AC/AF, NVC3/D7, F0/F1/106 or NV118/120/124/12B, GP102/104/107, I'm willing to pay for the shipment and customs.
[[Marcin Kościelnicki|Marcin Kościelnicki]] |  Poland  |  NV0A, NV2A, NV4C, NV6[78], NVC8, NVF0 - sorry, already got all non-tricky ones :) 
Samuel Pitoiset | Bordeaux, France | Any NV50 (apart from NV50, NV96, NV98, NVA0 and NVA8), any NVC0 (apart from NVC1 and NVC8), any NVE0, NV110 and NV130, I'm willing to pay for the shipment
"""]]

<a name="potentiallyuseful"></a> 
# Hardware potentially useful to developers

If no one seems to need your card, but your card happens to be "rare" (e.g. special outputs, rare model, ...), someone might still be interested in taking it to carry out special tests. Propose your card on the [[nouveau|http://lists.freedesktop.org/mailman/listinfo/nouveau]] mailing list or the IRC channel and see if a developer wants it. If it so happens, that no-one replies to you, take it as: _"Thank you, but developers are completely occupied with the hardware they already have."_ 

_Notes_ 

1- you will in most cases be expected to pay for the shipping 
